from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time
from .views import index


# Create your tests here.
class UnitTestStory8(TestCase):
    def test_url_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
    def test_landing_page_is_using_correct_html(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')
    def test_landing_page_exist(self):
        self.assertIsNotNone(index)
    def test_function_story8(self):
        found = resolve('/')
        self.assertEqual(found.func, index)
    
class FunctionalTestStory8(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(FunctionalTestStory8, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(FunctionalTestStory8, self).tearDown()

    def test_accordion(self):
        selenium = self.selenium
        selenium.get(self.live_server_url + "/")
        time.sleep(3)

        welcome = selenium.find_element_by_class_name('line').text
        self.assertIn("Faraz Abisha's Story 8", welcome)

        accordions = self.selenium.find_elements_by_class_name('accordion')
        self.assertEqual(len(accordions), 4)
        time.sleep(3)

        Acc1 = selenium.find_element_by_name('activities')
        Acc1Down = selenium.find_element_by_name('button1down')
        Acc1Up = selenium.find_element_by_name('button1up')
        time.sleep(3)

        Acc1.send_keys(Keys.RETURN)
        time.sleep(2)
        Acc1Down.send_keys(Keys.RETURN)
        time.sleep(2)
        Acc1Up.send_keys(Keys.RETURN)
        time.sleep(3)

