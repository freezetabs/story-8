from django.urls import path
from . import views

appname = 'story8'
urlpatterns = [
    path('', views.index, name='index')
]